﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ReducedAppExit
{
    public static class DatabaseProcesor
    {
        private static string connectionString()
        {
            return @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + System.IO.Directory.GetCurrentDirectory().ToString() + @"\Database.mdf;Integrated Security=True";
        }

        public static Boolean FeedDatabase(String fileName)
        {
            if (fileName.EndsWith(".csv"))
            {
                try
                {
                    CsvProccesor csvProccesor = new CsvProccesor(fileName);

                    CreateDatabaseTable(csvProccesor.TableName);
                    foreach (var item in csvProccesor.CsvToTransaction())
                    {
                        PushTransaction(item);
                    }
                    CreateAgregateViews(csvProccesor.TableName);
                    return true;
                }
                catch(Exception e)
                {
                    throw e;
                }
            }
            else
            {
                try
                {
                    FileProccesor csvProccesor = new FileProccesor(fileName);

                    CreateDatabaseTable(csvProccesor.TableName);
                    foreach (var item in csvProccesor.CsvToTransaction())
                    {
                        PushTransaction(item);
                    }
                    CreateAgregateViews(csvProccesor.TableName);
                    return true;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static Boolean ExtractFromDatabase(DateTime fromDate, DateTime toDate,String excelDirectory,String ConsigneeCounrt, String ShipperCountry, String Service, int Counter, Label label)
        {
            CreateExcelFileWithData(CalculateOnDatabase(RememberDatabaseFile.SelectProperTables(fromDate, toDate), ConsigneeCounrt, ShipperCountry, Service, Counter),label, excelDirectory);
            return true;

        }

        private static Boolean PushTransaction(String transaction)
        {
            using (SqlConnection connection = new SqlConnection(connectionString()))
            {
                SqlCommand command = new SqlCommand(transaction)
                {
                    Connection = connection
                };
                connection.Open();

                Console.WriteLine(command.ExecuteNonQuery());

                connection.Close();
            }
            return true;
        }

        private static void CreateDatabaseTable(String TableName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString()))
            {
                SqlCommand commandCreate = new SqlCommand(
                    "CREATE TABLE [dbo].[" + TableName + "](" +
                    "[Id]               INT IDENTITY(1, 1)         NOT NULL," +
                    "[ShipperAccount]   NCHAR(9)    NOT NULL," +
                    "[ConsigneeCountry] VARCHAR(10) NULL," +
                    "[ShipperCountry]   VARCHAR(10) NULL," +
                    "[Service]          VARCHAR(10) NULL," +
                    "[Date]             DATETIME     NULL," +
                    "PRIMARY KEY CLUSTERED([Id] ASC)" +
                    ");");
                SqlCommand commandDrop = new SqlCommand(
                    "TRUNCATE TABLE [" + TableName + "];");
                commandCreate.Connection = connection;
                commandDrop.Connection = connection;
                connection.Open();
                try
                {
                    Console.WriteLine(commandCreate.ExecuteNonQuery());
                    RememberDatabaseFile.WriteTableName(TableName);
                }
                catch
                {
                    Console.WriteLine(commandDrop.ExecuteNonQuery());
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private static void CreateAgregateViews(String TableName)
        {
            string dynamicQuery =
                "CREATE VIEW [V_" + TableName + "] " +
                "AS " +
                "SELECT T1.ShipperAccount,T1.ConsigneeCountry,T1.ShipperCountry,T1.Service,Count(*) AS 'Counter' " +
                "FROM[" + TableName + "] T1 " +
                "WHERE T1.ShipperAccount != '0' " +
                "GROUP BY T1.ShipperAccount,T1.ConsigneeCountry,T1.ShipperCountry,T1.Service";
            PushTransaction(dynamicQuery);
        }

        private static string CreateAgregateFunction(List<string> tabels, String ConsigneeCounrt, String ShipperCountry, String Service, int Counter)
        {
            bool IsFirstDone = false;
            string dynamicQuery =
                "SELECT T1.ShipperAccount,T1.ConsigneeCountry,T1.ShipperCountry,T1.Service,SUM(T1.Counter) " +
                "FROM(";
            foreach (var item in tabels)
            {
                if (!IsFirstDone)
                {
                    dynamicQuery +=
                        "SELECT ShipperAccount, ConsigneeCountry, ShipperCountry, Service, Counter " +
                        "FROM[V_" + item + "] " +
                        CreateConstrainText(ConsigneeCounrt, ShipperCountry, Service);
                }
                else
                {
                    dynamicQuery +=
                        "UNION ALL " +
                        "SELECT ShipperAccount, ConsigneeCountry, ShipperCountry, Service, Counter " +
                        "FROM[V_" + item + "] "+
                        CreateConstrainText(ConsigneeCounrt, ShipperCountry, Service);
                }
            }
            dynamicQuery +=
                ") AS T1 " +
                "GROUP BY T1.ShipperAccount,T1.ConsigneeCountry,T1.ShipperCountry,T1.Service " +
                "HAVING SUM(T1.Counter) > " + Counter;

            return dynamicQuery;
        }

        private static string CreateConstrainText(String ConsigneeCounrt, String ShipperCountry, String Service)
        {
            String ret = "";
            Boolean IsStarted = false;
            if (ConsigneeCounrt != "")
            {
                if (!IsStarted) ret = " WHERE ";
                ret += "ConsigneeCountry = '" + ConsigneeCounrt + "' ";
            }
            if (ShipperCountry != "")
            {
                if (!IsStarted)
                {
                    ret = " WHERE ";
                }
                else
                {
                    ret += " AND ";
                }
                ret += "ShipperCountry = '" + ShipperCountry + "' ";
            }
            if (Service != "")
            {
                if (!IsStarted)
                {
                    ret = " WHERE ";
                }
                else
                {
                    ret += " AND ";
                }
                ret += "Service = '" + Service + "' ";
            }
            return ret;
        }

        private static List<DataBaseModel> CalculateOnDatabase (List<String> tables, String ConsigneeCounrt, String ShipperCountry, String Service, int Counter)
        {
            List<DataBaseModel> ret = new List<DataBaseModel>();
            SqlCommand command = new SqlCommand(CreateAgregateFunction(tables, ConsigneeCounrt, ShipperCountry, Service, Counter));
            using (SqlConnection connection = new SqlConnection(connectionString()))
            {
                command.Connection = connection;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ret.Add(new DataBaseModel
                        {
                            ShipperAccount = reader.GetString(0),
                            ConsigneeCountry = reader.GetString(1),
                            ShipperCountry = reader.GetString(2),
                            Service = reader.GetString(3),
                            Counter = reader.GetInt32(4)
                        });
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close(); 
            }

            return ret;
        }

        private static void CreateExcelFileWithData(List<DataBaseModel> list,Label label, String excelDirectory)
        {
            String path = excelDirectory;

            var newFile = new FileInfo(path);
            using (ExcelPackage xlPackage = new ExcelPackage(newFile))
            {
                ExcelWorksheet ws = xlPackage.Workbook.Worksheets.Add("report");

                ws.Cells[1, 1].Value = "ID";
                ws.Cells[1, 2].Value = "Shipper Account";
                ws.Cells[1, 3].Value = "Consignee Country";
                ws.Cells[1, 4].Value = "Shipper Country";
                ws.Cells[1, 5].Value = "Service";
                ws.Cells[1, 6].Value = "Count";

                int i = 2;
                int id = 1;
                foreach (var item in list)
                {
                    label.Content = id.ToString();
                    ws.Cells[i, 1].Value = id;
                    ws.Cells[i, 2].Value = item.ShipperAccount;
                    ws.Cells[i, 3].Value = item.ConsigneeCountry;
                    ws.Cells[i, 4].Value = item.ShipperCountry;
                    ws.Cells[i, 5].Value = item.Service;
                    ws.Cells[i, 6].Value = item.Counter;
                    i++;
                }
                Console.WriteLine(i.ToString() + " rows to save");
                xlPackage.Save();
                Console.WriteLine("Done");
            }
        }
    }
}
