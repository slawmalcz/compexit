﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReducedAppExit
{
    public class DataBaseModel
    {
        public String ShipperAccount { get; set; }
        public String ConsigneeCountry { get; set; }
        public String ShipperCountry { get; set; }
        public String Service { get; set; }
        public int Counter { get; set; }
    }
}
