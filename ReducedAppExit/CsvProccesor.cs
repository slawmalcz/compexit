﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReducedAppExit
{
    class CsvProccesor
    {
        private string PathToFile;
        public string TableName;
        public CsvProccesor(string path)
        {
            PathToFile = path;
            TableName = DetermineTableName();
        }

        public List<String> CsvToTransaction()
        {
            List<String> ret = new List<string>();
            DateClase dateClase = new DateClase();
            using (StreamReader reader = new StreamReader(PathToFile))
            {
                String line;
                String editedLine = "";
                int i = 0;
                line = reader.ReadLine();


                while ((line = reader.ReadLine()) != null)
                {
                    if (i == 250)
                    {
                        i = 0;
                        ret.Add(editedLine);
                        editedLine = "";
                    }
                    if (i == 0)
                    {
                        editedLine = "INSERT INTO [dbo].["+TableName+"]([ShipperAccount],[ConsigneeCountry],[ShipperCountry],[Service],[Date])VALUES";
                    }

                    var result = line.Split('"').Select((element, index) => index % 2 == 0  // If even index
                        ? element.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)  // Split the item
                        : new string[] { element })  // Keep the entire item
                        .SelectMany(element => element).ToList();
                    if (result.Count == 44)
                    {
                        if(!(i==250||i==0))editedLine += ",";
                        editedLine += "('";
                        editedLine += result[17] + "','";
                        editedLine += result[15] + "','";
                        editedLine += result[25] + "','";
                        editedLine += result[32] + "','";
                        DateTime dateTime = new DateTime(
                            Int32.Parse("20" + result[6].Split('-')[2]), 
                            dateClase.GetNumOfMonth(result[6].Split('-')[1]),
                            Int32.Parse(result[6].Split('-')[0]));
                        editedLine += dateTime.ToString("yyyy-MM-dd") + "')";
                    }
                    i++;
                }
            }

            return ret;
        }

        private string DetermineTableName()
        {
            DateClase dateClase = new DateClase();
            using (StreamReader reader = new StreamReader(PathToFile))
            {
                String line;
                line = reader.ReadLine();
                line = reader.ReadLine();
                var result = line.Split('"').Select((element, index) => index % 2 == 0  // If even index
                        ? element.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)  // Split the item
                        : new string[] { element })  // Keep the entire item
                        .SelectMany(element => element).ToList();

                DateTime dateTime = new DateTime(
                            Int32.Parse("20" + result[6].Split('-')[2]),
                            dateClase.GetNumOfMonth(result[6].Split('-')[1]),
                            Int32.Parse(result[6].Split('-')[0]));
                return dateTime.ToString("yyyy-MM-dd") + "-DataTable";
            }
        }
    }
}
