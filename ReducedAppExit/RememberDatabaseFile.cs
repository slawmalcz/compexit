﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReducedAppExit
{
    class RememberDatabaseFile
    {
        private const String path = "datastack.txt";

        public static void WriteTableName(string tableName)
        {
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(tableName);
                }
            }
            else
            {
                try
                {
                    using (StreamReader sr = File.OpenText(path))
                    {
                        string s = "";
                        while ((s = sr.ReadLine()) != null)
                        {
                            if (s.Equals(tableName)) throw new Exception("Given table exist in record the table will be trunctuated");
                        }
                    }
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine(tableName);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }   
            }
        }

        public static List<String> SelectProperTables(DateTime fromDate, DateTime toDate)
        {
            List<String> ret = new List<string>();

            using (StreamReader sr = File.OpenText(path))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    DateTime TableDate = new DateTime(Int32.Parse(s.Split('-')[0]), Int32.Parse(s.Split('-')[1]), Int32.Parse(s.Split('-')[2]));
                    if (TableDate >= fromDate && TableDate <= toDate) ret.Add(s);
                }
            }

            return ret;
        }
    }
}
