﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReducedAppExit
{
    class DateClase
    {
        private Dictionary<String, int> MonthMap = new Dictionary<string, int>();

        public DateClase()
        {
            MonthMap.Add("Jan", 1);
            MonthMap.Add("Feb", 2);
            MonthMap.Add("Mar", 3);
            MonthMap.Add("Apr", 4);
            MonthMap.Add("May", 5);
            MonthMap.Add("Jun", 6);
            MonthMap.Add("Jul", 7);
            MonthMap.Add("Aug", 8);
            MonthMap.Add("Sep", 9);
            MonthMap.Add("Oct", 10);
            MonthMap.Add("Nov", 11);
            MonthMap.Add("Dec", 12);
            MonthMap.Add("JAM", 1);
            MonthMap.Add("FEB", 2);
            MonthMap.Add("MAR", 3);
            MonthMap.Add("APR", 4);
            MonthMap.Add("MAY", 5);
            MonthMap.Add("JUN", 6);
            MonthMap.Add("JUL", 7);
            MonthMap.Add("AUG", 8);
            MonthMap.Add("SEP", 9);
            MonthMap.Add("OCT", 10);
            MonthMap.Add("NOV", 11);
            MonthMap.Add("DEC", 12);
        }

        public int GetNumOfMonth(String toParse)
        {
            return MonthMap[toParse];
        }
    }
}
