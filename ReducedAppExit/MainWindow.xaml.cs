﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace ReducedAppExit
{
    public partial class MainWindow : Window
    {
        List<String> ChoosenFile = new List<string>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ChooseFiles_Click(object sender, RoutedEventArgs e)
        {
            String[] pathToFiles;

            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "Comma-seperated values |*.csv|All files(*.*)|*.*",
                Title = "Select a File",
                Multiselect = true,
                DefaultExt = ".csv"
            };

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pathToFiles = openFileDialog.FileNames;

                foreach(var item in pathToFiles)
                {
                    ChoosenFile.Add(item);
                    //DatabaseProcesor.FeedDatabase(item);
                }
            }
            else
            {
                // IDK
            }

            foreach(var item in ChoosenFile)
            {
                FileTextBox.Text = FileTextBox.Text + " " + item;
            }
        }

        private void FinalizeFeed_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in ChoosenFile)
            {
                try
                {
                    DatabaseProcesor.FeedDatabase(item);
                    UploadTextBlock.Text += "Succes: "+item+" \n";
                }
                catch (Exception exception)
                {
                    UploadTextBlock.Text += "Fail: "+item+" \n";
                    UploadTextBlock.Text += "   "+exception.Message+"\n";
                }
            }
        }

        private void ChooseDestinationButton_Click(object sender, RoutedEventArgs e)
        {
            String pathToFiles = "";

            SaveFileDialog openFileDialog = new SaveFileDialog
            {
                Filter = "Excel file |*.xlsx",
                Title = "Select destination Folder",
                DefaultExt = ".xlsx"
            };

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pathToFiles = openFileDialog.FileName;
            }
            else
            {
                // IDK
            }

            DestinationTextBox.Text = pathToFiles;
        }

        private void CreateExelButton_Click(object sender, RoutedEventArgs e)
        {
            DatabaseProcesor.ExtractFromDatabase(
                new DateTime(Int32.Parse(FromDateDataPicker.Text.Split('/')[2]), Int32.Parse(FromDateDataPicker.Text.Split('/')[1]), Int32.Parse(FromDateDataPicker.Text.Split('/')[0])),
                new DateTime(Int32.Parse(ToDateDatePicker.Text.Split('/')[2]), Int32.Parse(ToDateDatePicker.Text.Split('/')[1]), Int32.Parse(ToDateDatePicker.Text.Split('/')[0])),
                DestinationTextBox.Text,
                ConsigneeCountrTextBox.Text,
                ShipperCountryTextBox.Text,
                ServiceTextBox.Text,
                Int32.Parse(CounterTextBox.Text),
                OperationCounter
                );
        }
    }
}
