﻿using System;
using System.Collections.Generic;
using System.Linq;
using CompExit.ViewModel;
using OfficeOpenXml;
using System.IO;
using System.Threading.Tasks;

namespace CompExit.Programable
{
    class DataOperations
    {

        public static void WriteToDatabaseAsync(List<MainData> list,Entities db)
        {
            foreach(MainData item in list)
            {
                db.MainDatas.Add(item);
            }
                db.Configuration.AutoDetectChangesEnabled = false;
                db.Configuration.ValidateOnSaveEnabled = false;
                var tmp = db.SaveChanges();
                db.Configuration.AutoDetectChangesEnabled = true;
                db.Configuration.ValidateOnSaveEnabled = true;
        }

        public static List<OutputTable> ReadFromDatabase()
        {
            List<OutputTable> retrun = new List<OutputTable>();
            Console.WriteLine("Hello");
            using (ViewModel.Entities ctx = new ViewModel.Entities())
            {
                var ret = from t in ctx.OutputTables
                          select t;
                foreach (var t in ret)
                {
                    retrun.Add(t);
                }
            }
            return retrun;
        }

        

        public static List<MainData> ReadCSVFile(String pathString)
        {
            List<MainData> ret = new List<MainData>();

            using(var reader = new System.IO.StreamReader(pathString))
            {
                var line = reader.ReadLine();

                while (!reader.EndOfStream)
                {
                    line = reader.ReadLine();
                    String[] creationStringList = new String[44];
                    String[] splited = line.Split(',');
                    for (int i = 0; i < 44; i++)
                    {
                        creationStringList[i] = splited[i]; 
                    }
                    
                    ret.Add(RetMainDataInstance(creationStringList));
                }
            }

            return ret;
        }

        public static MainData RetMainDataInstance(String[] constructionList)
        {
            MainData ret = new MainData
            {
                AWB__ = constructionList[0],
                CRN__ = constructionList[1],
                ORIGIN = constructionList[2],
                DEST = constructionList[3],
                ORIGIN_CNTY = constructionList[4],
                EXPORT_CNTY = constructionList[5],
                SHIP_DATE = constructionList[6],
                CONSIGNEE_ACCOUNT = constructionList[7],
                CONSIGNEE_PHONE = constructionList[8],
                CONSIGNEE_COMPANY = constructionList[9],
                CONSIGNEE_NAME = constructionList[10],
                CONSIGNEE_ADDRESS1 = constructionList[11],
                CONSIGNEE_ADDRESS2 = constructionList[12],
                CONSIGNEE_CITY = constructionList[13],
                CONSIGNEE_STATE = constructionList[14],
                CONSIGNEE_COUNTRY = constructionList[15],
                CONSIGNEE_POSTAL = constructionList[16],
                SHIPPER_ACCOUNT = constructionList[17],
                SHIPPER_PHONE = constructionList[18],
                SHIPPER_COMPANY = constructionList[19],
                SHIPPER_NAME = constructionList[20],
                SHIPPER_ADDRES1 = constructionList[21],
                SHIPPER_ADDRES2 = constructionList[22],
                SHIPPER_CITY = constructionList[23],
                SHIPERR_STATE = constructionList[24],
                SHIPPER_COUNTRY = constructionList[25],
                SHIPPER_POSTAL = constructionList[26],
                BROKER_NAME = constructionList[27],
                BROKER_PHONE = constructionList[28],
                BROKER_CITY = constructionList[29],
                BROKER_COUNTRY = constructionList[30],
                BROKER_CUSTOMS_ID = constructionList[31],
                SERVICE = constructionList[32],
                SERVICE_NEW = constructionList[33],
                PACKAGE_TYPE = constructionList[34],
                BILL_TO = constructionList[35],
                BILL_DUTY = constructionList[36],
                TOTAL = constructionList[37],
                TOTAL_WGL = constructionList[38],
                CURR = constructionList[39],
                CUSTOM_VAL = constructionList[40],
                NDR = constructionList[41],
                SHIPMENT_DESCRIPTION = constructionList[42],
                OFFL_USE = constructionList[43]
            };

            return ret;
        }

        public static void CreateExcelFileWithData(List<OutputTable> list)
        {
            String path = System.IO.Directory.GetCurrentDirectory() + @"\testSheat.xlsx";

            var newFile = new FileInfo(path);
            using (ExcelPackage xlPackage = new ExcelPackage(newFile))
            {
                ExcelWorksheet ws = xlPackage.Workbook.Worksheets.Add("report");

                ws.Cells[1, 1].Value = "ID";
                ws.Cells[1, 2].Value = "Shipper Account";
                ws.Cells[1, 3].Value = "Consignee Country";
                ws.Cells[1, 4].Value = "Shipper Country";
                ws.Cells[1, 5].Value = "Service";
                ws.Cells[1, 6].Value = "Count";

                int i = 2;
                foreach (var item in list)
                {
                    ws.Cells[i, 1].Value = item.ID;
                    ws.Cells[i, 2].Value = item.ConsigneeAccount;
                    ws.Cells[i, 3].Value = item.ConsigneeCountry;
                    ws.Cells[i, 4].Value = item.ShipperCountry;
                    ws.Cells[i, 5].Value = item.Service;
                    ws.Cells[i, 6].Value = item.Count;
                    i++;
                }
                Console.WriteLine(i.ToString() + " rows to save");
                xlPackage.Save();
                Console.WriteLine("Done");
            }
        }

        internal static void CalculateOnDatabase(Entities dataBase,String service,String consigneeCountry,String shipperCountry,String having)
        {
            string dynamicSQL =
                "INSERT INTO dbo.OutputTable " +
                    "SELECT DISTINCT(T1.[SHIPPER ACCOUNT]), T1.[CONSIGNEE COUNTRY], T1.[SHIPPER COUNTRY], T1.SERVICE, T3.COUNTING " +
                    "FROM dbo.MainData T1 " +
                        "INNER JOIN( " +
                        "SELECT T2.[SHIPPER ACCOUNT], Count(*) AS 'COUNTING' " +
                        "FROM dbo.MainData T2 " +
                        CreateWhere(true,service,consigneeCountry,shipperCountry,having) +
                        "GROUP BY T2.[SHIPPER ACCOUNT] " +
                        ") T3 ON " +
                        "T1.[SHIPPER ACCOUNT] = T3.[SHIPPER ACCOUNT] " +
                "WHERE T1.[SHIPPER ACCOUNT] IN(SELECT DISTINCT T4.[SHIPPER ACCOUNT] FROM dbo.MainData T4 WHERE T4.[SHIPPER ACCOUNT] LIKE '_________') " +
                    CreateWhere(false, service, consigneeCountry, shipperCountry, having) +
                "ORDER BY T3.COUNTING DESC ";
            dataBase.Database.ExecuteSqlCommand(dynamicSQL);
        }

        internal static void CleanDatabase(Entities dataBase)
        {
            string dynamicSQL =
                "DELETE FROM [dbo].[OutputTable]";
            dataBase.Database.ExecuteSqlCommand(dynamicSQL);
        }

        private static string CreateWhere(Boolean IsInner, String service, String consigneeCountry, String shipperCountry, String having)
        {
            string ret = "";
            if (IsInner)
            {
                Boolean IsBeginExists = false;
                if (consigneeCountry.Length > 0)
                {
                    ret = " WHERE   T2.[CONSIGNEE COUNTRY] = '" + consigneeCountry + "' ";
                    IsBeginExists = true;
                }
                if(shipperCountry.Length > 0)
                {
                    if (!IsBeginExists)
                    {
                        ret = " WHERE T2.[SHIPPER COUNTRY] = '" + shipperCountry + "' ";
                        IsBeginExists = true;
                    }
                    ret += " AND T2.[SHIPPER COUNTRY] = '" + shipperCountry + "' ";
                }
                if (service.Length > 0)
                {
                    if (!IsBeginExists)
                    {
                        ret = " WHERE T2.SERVICE = '" + service + "' ";
                        IsBeginExists = true;
                    }
                    ret += " AND T2.SERVICE = '" + service + "' ";
                }
            }
            else
            {
                if (having.Length > 0)ret += " AND T3.COUNTING > " + having + " ";
                if (consigneeCountry.Length > 0) ret += " AND T1.[CONSIGNEE COUNTRY] = '" + consigneeCountry + "' ";
                if (shipperCountry.Length > 0) ret += " AND T1.[SHIPPER COUNTRY] = '" + shipperCountry + "' ";
                if (service.Length > 0) ret += " AND T1.SERVICE = '" + service + "' ";
            }
            return ret;
        }
    }
}
