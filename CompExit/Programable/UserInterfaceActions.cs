﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompExit.ViewModel;

namespace CompExit.Programable
{
    static class UserInterfaceActions
    {
        public static List<String> LoadPath(String path)
        {
            List<String> ret = new List<String>();
            if (IsDirectory(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "*.csv");
                foreach(var file in files)
                {
                    ret.Add(file);
                }
            }
            else if(File.Exists(path))
            {
                ret.Add(path);
            }
            else
            {
                throw new Exception("Given pathi is not valid");
            }
            return ret;
        }

        public static Boolean IsDirectory(String path)
        {
            try
            {
                string[] subfolders = Directory.GetDirectories(path);
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }

        public static void InsertIntoDatabase(String path, Entities db)
        {
            int i = 0;


            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    String line;
                    String editedLine;
                    int k=0;

                    while ((line = reader.ReadLine()) != null)
                    {
                        if (i != 0)
                        {
                            editedLine = "INSERT INTO [dbo].[MainData]([AWB #],[CRN #],[ORIGIN],[DEST],[ORIGIN CNTY],[EXPORT CNTY],[SHIP DATE],[CONSIGNEE ACCOUNT],[CONSIGNEE PHONE],[CONSIGNEE COMPANY],[CONSIGNEE NAME],[CONSIGNEE ADDRESS1],[CONSIGNEE ADDRESS2],[CONSIGNEE CITY],[CONSIGNEE STATE],[CONSIGNEE COUNTRY],[CONSIGNEE POSTAL],[SHIPPER ACCOUNT],[SHIPPER PHONE],[SHIPPER COMPANY],[SHIPPER NAME],[SHIPPER ADDRES1],[SHIPPER ADDRES2],[SHIPPER CITY],[SHIPERR STATE],[SHIPPER COUNTRY],[SHIPPER POSTAL],[BROKER NAME],[BROKER PHONE],[BROKER CITY],[BROKER COUNTRY],[BROKER CUSTOMS ID],[SERVICE],[SERVICE NEW],[PACKAGE TYPE],[BILL TO],[BILL DUTY],[TOTAL],[TOTAL WGL],[CURR],[CUSTOM VAL],[NDR],[SHIPMENT DESCRIPTION],[OFFL USE])VALUES";
                            int toCheck = ("[AWB #],[CRN #],[ORIGIN],[DEST],[ORIGIN CNTY],[EXPORT CNTY],[SHIP DATE],[CONSIGNEE ACCOUNT],[CONSIGNEE PHONE],[CONSIGNEE COMPANY],[CONSIGNEE NAME],[CONSIGNEE ADDRESS1],[CONSIGNEE ADDRESS2],[CONSIGNEE CITY],[CONSIGNEE STATE],[CONSIGNEE COUNTRY],[CONSIGNEE POSTAL],[SHIPPER ACCOUNT],[SHIPPER PHONE],[SHIPPER COMPANY],[SHIPPER NAME],[SHIPPER ADDRES1],[SHIPPER ADDRES2],[SHIPPER CITY],[SHIPERR STATE],[SHIPPER COUNTRY],[SHIPPER POSTAL],[BROKER NAME],[BROKER PHONE],[BROKER CITY],[BROKER COUNTRY],[BROKER CUSTOMS ID],[SERVICE],[SERVICE NEW],[PACKAGE TYPE],[BILL TO],[BILL DUTY],[TOTAL],[TOTAL WGL],[CURR],[CUSTOM VAL],[NDR],[SHIPMENT DESCRIPTION],[OFFL USE]").Split(',').Length;
                            editedLine += "(";
                            int j = 0;
                            var result = line.Split('"')
                     .Select((element, index) => index % 2 == 0  // If even index
                                           ? element.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)  // Split the item
                                           : new string[] { element })  // Keep the entire item
                     .SelectMany(element => element).ToList();
                            foreach (var chunk in result)
                            { 
                                if (j != 0) { editedLine += ","; }
                                editedLine += @"'" + chunk + @"'";
                                j++;
                            }
                            editedLine += ")";

                            try
                            {
                                db.Database.ExecuteSqlCommand(editedLine);
                                k++;
                            }
                            catch (Exception)
                            {
                            }
                        }
                        i++;
                                                }

                    Console.WriteLine("test");
                }
            }

        }
    }
}
