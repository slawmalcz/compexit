﻿using CompExit.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows;

namespace CompExit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region DataInserts

        List<DataModels.CSVDataModel> CsvFiles = new List<DataModels.CSVDataModel>();
        Entities DataBase = new ViewModel.Entities();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Load_Data_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var test in Programable.UserInterfaceActions.LoadPath(PathFinder.Text))
                {
                    AddFileToProcces(new DataModels.CSVDataModel(test), test);
                }
            }catch(Exception excep)
            {
                ListOfWork.Text += "[FAILD] to load given path { " + excep.Message + " } \n";
            }
            
        }

        private void ClearData_Click(object sender, RoutedEventArgs e)
        {
            CsvFiles.Clear();
            ListOfWork.Text = "";
            PathFinder.Text = "C:\\";
        }

        private  void FeedDatabase_Click(object sender, RoutedEventArgs e)
        {
            long Performancetest = 0;

            var watchTime = System.Diagnostics.Stopwatch.StartNew();

            if (CsvFiles.ToArray().Length > 0)
            {
                foreach (var item in CsvFiles)
                {
                    if (item.status)
                    {
                        //item.CreateFileRead();
                        //Programable.DataOperations.WriteToDatabaseAsync(item.DataList,DataBase);
                        Programable.UserInterfaceActions.InsertIntoDatabase(item.Path,DataBase);
                    }
                    item.DataList = null;
                }
            }

            watchTime.Stop();
            Performancetest = watchTime.ElapsedMilliseconds;

            ListOfWork.Text = "SUCCESFULL FEED in: "+ Performancetest.ToString();
        }

        private void AddFileToProcces(DataModels.CSVDataModel toAdd,String path)
        {
            String tempListOfWorkText = ListOfWork.Text;
            try
            {
                foreach (var repeatTest in CsvFiles) if (repeatTest.Path == path) throw new Exception("Data present in current query");
                CsvFiles.Add(toAdd);
                ListOfWork.Text = tempListOfWorkText + "[SUCCES] " + path + "\n";
            }
            catch (Exception excep)
            {
                ListOfWork.Text = tempListOfWorkText + "[FAILD]  " + path + " { " + excep.Message + " } \n";
            }
        }

        #endregion

        private void CreateFileButton_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("GO");
            OutTBlock.Text = "";

            try
            {
                Programable.DataOperations.CalculateOnDatabase(DataBase, ServiceTBox.Text, ConsigneeCountryTBox.Text, ShipperCountryTBox.Text, CountTBox.Text);
                Programable.DataOperations.CreateExcelFileWithData(Programable.DataOperations.ReadFromDatabase());
                Programable.DataOperations.CleanDatabase(DataBase);
                OutTBlock.Text = "[SUCESS]";
            }
            catch (Exception ecep)
            {
                OutTBlock.Text = "[Fail] " + ecep.Message;
            }
            
        }
    }
}
