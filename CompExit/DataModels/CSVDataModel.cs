﻿using CompExit.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompExit.DataModels
{
    public class CSVDataModel
    {
        public List<MainData> DataList { get; set;}
        public String Path { get; set;}
        public int NumberOfLinesProcesed {
            get {
                return DataList.ToArray().Length;
            }
            private set { }
        }
        public bool status = false;

        public CSVDataModel(string path)
        {
            this.Path = path;
            if (File.Exists(Path))
            {
                if (Path.Contains(".csv"))
                {
                    
                    status = true;
                }
                else
                {
                    throw new Exception("File have no .csv extension");
                }
            }
            else
                throw new Exception("File don't exist");
        }

        private CSVDataModel(List<MainData> dataList, string path, int numberOfLinesProcesed)
        {
            DataList = dataList;
            this.Path = path;
            NumberOfLinesProcesed = numberOfLinesProcesed;
        }

        public void CreateFileRead()
        {
            if (status)
            {
                DataList = Programable.DataOperations.ReadCSVFile(Path); 
            }
        }
    }
}
